package com.example.petclinic.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

public class OwnerTests {
	
	@Test
	public void testOwnerConstructor() {
		Owner owner = new Owner("Ravi", LocalDate.of(1982, 5, 23));
		assertNotNull(owner);
		assertEquals(owner.getName(), "Ravi");
	}
	
	@Test
	public void testSetName() {
		Owner owner = new Owner("Harish", LocalDate.of(1982, 5, 23));
		owner.setName("Vinod");
		assertEquals(owner.getName(), "Vinod");
	}
	
	@Test
	public void testGetName() {
		Owner owner = new Owner("Harish", LocalDate.of(1982, 5, 23));
		assertEquals(owner.getName(), "Harish");
	}
	
	@Test
	public void testHashCode() {
		Owner owner1 = new Owner("Harish", LocalDate.of(1982, 5, 23));
		Owner owner2 = new Owner("Harish", LocalDate.of(1982, 5, 23));
		
		int hashCode1 = owner1.hashCode();
		int hashCode2 = owner2.hashCode();
		
		assertEquals(hashCode1, hashCode2);
	}
	
	@Test
	public void testNegativeHashCode() {
		Owner owner1 = new Owner("Vinayak", LocalDate.of(1988, 5, 23));
		Owner owner2 = new Owner("Harish", LocalDate.of(1982, 5, 23));
		
		int hashCode1 = owner1.hashCode();
		int hashCode2 = owner2.hashCode();
		
		assertNotEquals(hashCode1, hashCode2);
	}
	
	@Test
	public void testEquals() {
		Owner owner1 = new Owner("Harish", LocalDate.of(1982, 5, 23));
		Owner owner2 = new Owner("Harish", LocalDate.of(1982, 5, 23));
		
		assertFalse(owner1 == owner2);
		assertTrue(owner1.equals(owner2));
	}
	
	@Test
	public void testNegativeEquals() {
		Owner owner1 = new Owner("Vinayak", LocalDate.of(1988, 5, 23));
		Owner owner2 = new Owner("Harish", LocalDate.of(1982, 5, 23));
		
		assertFalse(owner1 == owner2);
		assertFalse(owner1.equals(owner2));

	}
	
	@Test
	void addPetTest() {
		Owner owner = new Owner("Rakshit", LocalDate.of(1988, 5, 23));
		Pet pet = new Pet("susy");
		owner.addPet(pet);
		
		assertEquals(owner.getPets().size(), 1);
		assertEquals(owner.getPets().get(0).getName(), "susy");
	}

}
