package com.example.petclinic.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import com.example.petclinic.model.Owner;
import com.example.petclinic.repository.OwnerJPARepository;

@ExtendWith(MockitoExtension.class)
public class OwnerServiceTests {
	
	@Mock
	private OwnerJPARepository ownerJPARepository;
	
	@InjectMocks
	private OwnerService ownerService;
	
	
	@Test
	void testConstructor() {
		OwnerService ownerService = new OwnerService(null);
		assertNotNull(ownerService);
	}
	
	@Test
	void testConstructorWithRepository() {
		OwnerService ownerService = new OwnerService(null);
		assertNotNull(ownerService);
	}
	
	@Test
	void testSaveOwner() {
		assertNotNull(this.ownerService);
		//template
		// set the expectation
		Owner owner = new Owner("Rajiv", LocalDate.of(1988, 12, 12));
		when(this.ownerJPARepository.save(owner)).thenReturn(owner);
		
		//execute the code
		Owner savedOwner = this.ownerService.saveOwner(owner);
		
		//assertions
		assertNotNull(savedOwner);
		assertEquals(savedOwner.getName(), "Rajiv");
		
		//verification - the expectations were met
		verify(this.ownerJPARepository, times(1)).save(owner);
	}
	
	@Test
	void testFetchAllOwners() {
		Pageable pageRequest = PageRequest.of(1, 10);
		
		Page<Owner> page = new Page() {

			@Override
			public int getNumber() {
				// TODO Auto-generated method stub
				return 100;
			}

			@Override
			public int getSize() {
				// TODO Auto-generated method stub
				return 10;
			}

			@Override
			public int getNumberOfElements() {
				// TODO Auto-generated method stub
				return 10;
			}

			@Override
			public List getContent() {
				// TODO Auto-generated method stub
				return new ArrayList<>();
			}

			@Override
			public boolean hasContent() {
				// TODO Auto-generated method stub
				return true;
			}

			@Override
			public Sort getSort() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isFirst() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isLast() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return true;
			}

			@Override
			public boolean hasPrevious() {
				// TODO Auto-generated method stub
				return true;
			}

			@Override
			public Pageable nextPageable() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Pageable previousPageable() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Iterator iterator() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public int getTotalPages() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public long getTotalElements() {
				// TODO Auto-generated method stub
				return 10;
			}

			@Override
			public Page map(Function converter) {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
		
		
		when(this.ownerJPARepository.findAll(pageRequest)).thenReturn(page);
		Map<String, Object> response = this.ownerService.fetchAllOwners(10, 1);
		
		assertNotNull(response);
		assertEquals(response.get("total-records"), 10L);
		assertEquals(response.get("size"),10);
		assertNotNull(response.get("data"));

		//verification
		verify(this.ownerJPARepository, times(1)).findAll(pageRequest);

	}
	
	@Test
	void testFindById() {
		Owner owner = new Owner("Rajiv", LocalDate.of(1988, 12, 12));

		when(this.ownerJPARepository.findById(12)).thenReturn(Optional.ofNullable(owner));
		Owner response = this.ownerService.findOwnerById(12);
		
		assertNotNull(response);
		
		//verification
		verify(this.ownerJPARepository, times(1)).findById(12);
	}
	
	@Test
	void testNegativeFindById() {
		when(this.ownerJPARepository.findById(12)).thenReturn(Optional.ofNullable(null));
		try {
			Owner response = this.ownerService.findOwnerById(12);	
		} catch(Exception exception) {
			assertNotNull(exception);
			assertTrue(exception instanceof IllegalArgumentException);
		}
		//verification
		verify(this.ownerJPARepository, times(1)).findById(12);
	}
	
	@Test
	void testNegativeDeleteById() {
		doNothing().when(ownerJPARepository).deleteById(isA(Integer.class));
		try {
			this.ownerService.deleteOwnerById(12);	
		} catch(Exception exception) {
			fail("Should not throw any exception ::");
		}
		//verification
		verify(this.ownerJPARepository, times(1)).deleteById(12);
	}

}
