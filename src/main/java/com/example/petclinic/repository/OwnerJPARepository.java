package com.example.petclinic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.petclinic.model.Owner;

@Repository
public interface OwnerJPARepository extends JpaRepository<Owner, Integer>{

}
