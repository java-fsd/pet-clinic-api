package com.example.petclinic.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import javax.validation.constraints.PastOrPresent;

import java.time.LocalDate;
import java.util.*;

@Entity
@Table(name="owner")
public class Owner {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@NotBlank(message = "name cannot be blank")
	private String name;
	
	@OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Pet> pets = new ArrayList<>();
	
	@PastOrPresent(message = "dob cannot be in the past date")
	private LocalDate dob;
	
	private Owner() {}
	
	public Owner(String name, LocalDate dob) {
		this.name = name;
		this.dob = dob;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Pet> getPets() {
		return pets;
	}

	public void setPets(List<Pet> pets) {
		this.pets = pets;
	}

	public int getId() {
		return id;
	}
	
	
	
	public LocalDate getDob() {
		return dob;
	}

	public void setDob(LocalDate dob) {
		this.dob = dob;
	}

	public void addPet(Pet pet) {
		this.pets.add(pet);
		pet.setOwner(this);
	}

	@Override
	public int hashCode() {
		return Objects.hash(dob, id, name, pets);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Owner other = (Owner) obj;
		return Objects.equals(dob, other.dob) && id == other.id && Objects.equals(name, other.name)
				&& Objects.equals(pets, other.pets);
	}

}
