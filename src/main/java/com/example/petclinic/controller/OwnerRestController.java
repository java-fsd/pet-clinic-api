package com.example.petclinic.controller;

import java.util.Map;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.example.petclinic.model.Owner;
import com.example.petclinic.service.OwnerService;

@RestController
@RequestMapping("/api/owners")
//@CrossOrigin(origins = "*")
public class OwnerRestController {
	
	private final OwnerService ownerService;
	
	public OwnerRestController(OwnerService ownerService) {
		this.ownerService = ownerService;
	}
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public Map<String, Object> fetchAllOwners(
			@RequestParam(name = "size",defaultValue = "10", required = false) int recordsPerPage,
			@RequestParam(name = "page",defaultValue = "0", required = false) int pageNo){
		return this.ownerService.fetchAllOwners(recordsPerPage, pageNo);
	}
	
	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Owner fetchOwnerByOwnerId(@PathVariable("id") int id){
		return this.ownerService.findOwnerById(id);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Owner saveOwner(@RequestBody @Valid Owner owner) {
		owner.getPets().forEach(pet -> pet.setOwner(owner));
		return this.ownerService.saveOwner(owner);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteOwnerByOwnerId(@PathVariable("id") int id) {
		this.ownerService.deleteOwnerById(id);
	}
	
}
