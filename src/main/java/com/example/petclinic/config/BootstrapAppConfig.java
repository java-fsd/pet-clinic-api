package com.example.petclinic.config;

import java.time.ZoneId;
import java.util.stream.IntStream;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import com.example.petclinic.model.Owner;
import com.example.petclinic.model.Pet;
import com.example.petclinic.repository.OwnerJPARepository;
import com.github.javafaker.Faker;

@Configuration
public class BootstrapAppConfig {
	private final OwnerJPARepository ownerRepository;
	private Faker faker = new Faker();
	
	public BootstrapAppConfig(OwnerJPARepository ownerRepository) {
		this.ownerRepository = ownerRepository;
	}
	
	@EventListener(ApplicationReadyEvent.class)
	public void onApplicationReady(ApplicationReadyEvent appReadeyEvent) {
		System.out.println("Applicaation is ready");
		IntStream.range(1,  2000).forEach(index -> {
			
			Owner owner = new Owner(faker.name().firstName(), faker.date().birthday(18, 25).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
			IntStream.range(0, 3)
				.forEach(value -> {
					Pet pet = new Pet(faker.cat().name());
					owner.addPet(pet);
				});
			this.ownerRepository.save(owner);
		});
	}

}
