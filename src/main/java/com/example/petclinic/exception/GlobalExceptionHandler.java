package com.example.petclinic.exception;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public Error handleInvalidOrderId(IllegalArgumentException exception) {
		System.out.println("Exception while fetching the order with order id : "+ exception.getMessage());
		return new Error(12, exception.getMessage());
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public List<String> handleDataValidationError(MethodArgumentNotValidException exception) {
		System.out.println("Exception while positing the invalid data");
		List<ObjectError> errors = exception.getAllErrors();
		List<String> errorMessages = errors
			.stream()
			.map(error -> error.getDefaultMessage())
			.collect(Collectors.toList());
		return errorMessages;
	}

}


class Error {
	private int code;
	private String message;
	
	public Error(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		return "Error [code=" + code + ", message=" + message + "]";
	}
	
	
}