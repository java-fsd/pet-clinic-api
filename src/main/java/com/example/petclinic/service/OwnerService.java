package com.example.petclinic.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.example.petclinic.model.Owner;
import com.example.petclinic.repository.OwnerJPARepository;

@Service
public class OwnerService {
	
	private final OwnerJPARepository ownerJPARepository;
	
	public OwnerService(OwnerJPARepository ownerJPARepository) {
		this.ownerJPARepository = ownerJPARepository;
	}
	
	public Owner saveOwner(Owner owner) {
		return this.ownerJPARepository.save(owner);
	}
	
	public Map<String, Object> fetchAllOwners(int recordsPerPage, int pageNo){
		
		PageRequest pageRequest = PageRequest.of(pageNo, recordsPerPage);
		
		Page<Owner> pageResponse = this.ownerJPARepository.findAll(pageRequest);
		
		int numberOfElements = pageResponse.getNumberOfElements();
		long totalNumberOfRecords = pageResponse.getTotalElements();
		List<Owner> owners = pageResponse.getContent();
		
		
		Map<String, Object> response = new LinkedHashMap<>();
		response.put("total-records", totalNumberOfRecords);
		response.put("size", numberOfElements);
		response.put("data", owners);
		
		return response;
	}
	
	public Owner findOwnerById(int id) {
		return this.ownerJPARepository.findById(id).orElseThrow(() -> new IllegalArgumentException("invalid owner id"));
	}
	
	public void deleteOwnerById(int id) {
		this.ownerJPARepository.deleteById(id);
	}

}
